bl_info = {
    "name": "Import an STL assembly",
    "author": "Sussch",
    "version": (0, 1, 0),
    "blender": (2, 76, 0),
    "description": "Import a directory of STL files, "
                   "remap their names and assign materials.",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "",
    "category": "Import-Export"}


import os, re
import time
import math
import bpy
from bpy.props import (StringProperty, 
                       PointerProperty)
from bpy.types import (Panel,
                       Operator,
                       AddonPreferences,
                       PropertyGroup)
import bmesh
import mathutils
import sqlite3
import traceback


def round_to_n(x, significant=5, epsilon=1e-6):
    """Round to a specific number of significant digits.
    https://stackoverflow.com/a/36240131/1692112 """
    if x == 0.0:
        return 0.0
    return round(x, significant - \
        int(math.floor(math.log10(max(abs(x), abs(epsilon))))) - 1)


def bl_to_dict(bl_object, ignore_getters=True, subset=None):
    """Generic Blender object to dict converter."""
    skip_attributes = ['__doc__', '__module__', '__slots__']
    supported_basetypes = [
        "<class 'bool'>", 
        "<class 'int'>", 
        "<class 'float'>", 
        "<class 'str'>", 
        "<class 'list'>",
        "<class 'set'>"
        ]
    supported_casts = {
        "<class 'Color'>": 'list'
    }
    # TODO:: Figure out how to support bpy_prop_array, bpy_prop_collection, etc.
    # TODO:: Perhaps recurse on members of more complex classes.
    d = {}

    if not subset:
        subset = dir(bl_object)
    attributes = [x for x in subset if x not in skip_attributes]
    for name in attributes:
        # Get attribute value.
        val = getattr(bl_object, name)
        # Ignore getter properties.
        if ignore_getters:
            try:
                setattr(bl_object, name, val)
            except AttributeError as err:
                continue
        # Check against supported types.
        type_str = str(type(val))
        if type_str in supported_basetypes:
            d[name] = val
        elif type_str in supported_casts:
            d[name] = eval(supported_casts[type_str])(val)
        #else:
        #    print("Type {} of attribute {} not supported yet.".format(type(val), name))
    return d


def dict_to_bl(bl_obj, bl_dict, subset=None):
    """Update a Blender object from a dict."""
    for key, val in bl_dict.items():
        if not subset or (subset and key in subset):
            try:
                setattr(bl_obj, key, val)
            except AttributeError as err:
                continue
    return bl_obj


class STLAssemblyImportOperator(Operator):
    bl_idname = "import_stl_asm.import"
    bl_label = "Import an STL assembly directory"
    
    def execute(self, context):
        badprefix = "EC2.00.!ASM_Main - "
        dirpath = context.scene.stl_asm_import.path
        print("Importing {}..".format(dirpath))
        
        for fname in os.listdir(dirpath):
            if re.search(r'\.stl$', fname, re.I):
                # Shorten filenames by removing the constant prefix.
                if fname.startswith(badprefix):
                    fpath_orig = os.path.join(dirpath, fname)
                    fname = fname.replace(badprefix, '', 1)
                    fpath_new = os.path.join(dirpath, fname)
                    os.rename(fpath_orig, fpath_new)
                # Get full path to the STL file and import it.
                fpath = os.path.join(dirpath, fname)
                bpy.ops.import_mesh.stl(filepath=fpath)
        
        print("Storing original object names..")
        # Store the original names of the objects:
        for o in context.scene.objects:
            try:
                if o:
                    o.id = o.name
                else:
                    print("We've got a None object")
            except Exception as e:
                print("Failed to get object name\n{}".format(traceback.format_exc()))
        
        return {'FINISHED'}


class STLAssemblyProcessOperator(Operator):
    bl_idname = "import_stl_asm.process"
    bl_label = "Process the assembly"
    
    def find_planes(self, bm):
        """Find indices of vertices that belong to parallel planes."""
        d = {}
        for i in range(len(bm.faces)):
            p = bm.faces[i]
            # Create a hashable tuple from the normal vector.
            nt = mathutils.Vector(p.normal).to_tuple(5)
            # Convert negative zero to positive zero.
            nt = tuple([0.0 if x == -0.0 else x for x in nt])
            # Dict it, while ensuring that there aren't any duplicates.
            if nt not in d:
                d[nt] = set(p.verts)
            else:
                d[nt].update(set(p.verts))
        return d
    
    def fix_thin_features(self, obj, threshold):
        # Create a wrapped C++ class based on the actual mesh.
        bm = bmesh.new()
        bm.from_mesh(obj.data)
        bm.faces.ensure_lookup_table()
        
        # Find vertices on coplanar planes.
        planes = self.find_planes(bm)
        
        print("Num parallel planes: {}".format(len(planes)))
        for key, val in planes.items():
            # Normal vector that can be used in calculations.
            n_v = mathutils.Vector(key)
            # Convert negative zero to positive zero.
            n_i = tuple([-x if x != 0.0 else x for x in key])
            # Do we have an opposite plane?
            if n_i not in planes:
                continue
            
            # Vertices on opposite planes.
            plane_verts = list(val)
            plane_verts_i = list(planes[n_i])
            
            # Find minimum distance between the planes.
            d_min = None
            for v1 in plane_verts:
                p1 = v1.co
                
                for v2 in plane_verts_i:
                    p2 = v2.co
            
                    # Projected distance between the points.
                    d = mathutils.geometry.distance_point_to_plane(p1, p2, n_v)
                    # Check against thin slits in concave bodies.
                    if d < 0:
                        continue
                    # Remember the minimum distance.
                    if not d_min or d < d_min:
                        d_min = d
            
            # Thin beyond threshold?
            if d_min and d_min < threshold:
                print("Dist {}, {}".format(d, list(n_v)))
                # Translate the vertices on both planes away from each-other.
                bmesh.ops.translate(bm, vec=(n_v * 0.5 * threshold), verts=plane_verts)
                bmesh.ops.translate(bm, vec=(-n_v * 0.5 * threshold), verts=plane_verts_i)
        
        # Apply the changes to the actual mesh and free the C++ class.
        bm.to_mesh(obj.data)
        bm.free()
        obj.data.update()
    
    def execute(self, context):
        # Loop through objects.
        for o in context.selected_objects:
            self.fix_thin_features(o, 0.5)
        
        return {'FINISHED'}


class STLAssemblySaveOperator(Operator):
    bl_idname = "import_stl_asm.save"
    bl_label = "Process objects and materials in the assembly."
    
    def store_uids(self, context):
        """Store object identifiers in the Blender file."""
        wm = context.window_manager
        wm["objects"] = 0
        rna = wm.get("_RNA_UI", {})
        rna["objects"] = {o.name: o.id for o in bpy.data.objects}
        wm["objects"] = len(rna["objects"])
        wm["RNA_UI"] = rna
    
    def db_connect(self, context):
        """Connect to the database."""
        path = context.scene.stl_asm_import.path_db
        path = os.path.abspath(bpy.path.abspath(path))

        db = sqlite3.connect(path)
        db_cur = db.cursor()
        return db, db_cur

    def db_create_tables(self, db, db_cur):
        """Create tables in the database."""
        # In order to avoid the mess of content editing, 
        # shall drop the tables and recreate them. Otherwise it would
        # become exceptionally messy if materials or objects are removed.
        
        tables = ['objects', 'materials', 'material_map', 'modifiers', 'polys']
        for tab in tables:
            db_cur.execute("DROP TABLE IF EXISTS {};".format(tab))
        
        db_cur.execute("CREATE TABLE objects"
                       "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                       "id_name TEXT UNIQUE NOT NULL, "
                       "name TEXT UNIQUE);")
        db_cur.execute("CREATE TABLE materials"
                       "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                       "name TEXT UNIQUE NOT NULL, "
                       "code TEXT);")
        db_cur.execute("CREATE TABLE material_map"
                       "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                       "object_id INTEGER NOT NULL, "
                       "material_id INTEGER NOT NULL, "
                       "UNIQUE(object_id, material_id), "
                       "FOREIGN KEY(object_id) REFERENCES objects(id), "
                       "FOREIGN KEY(material_id) REFERENCES materials(id));")
        db_cur.execute("CREATE TABLE modifiers"
                       "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                       "name TEXT, type TEXT NOT NULL, "
                       "object_id INTEGER NOT NULL, "
                       "code TEXT NOT NULL, "
                       "UNIQUE(object_id, name));")
        db_cur.execute("CREATE TABLE polys"
                       "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                       "object_id INTEGER NOT NULL, "
                       "poly_index INTEGER NOT NULL, "
                       "mat_index INTEGER NOT NULL, "
                       "use_smooth INTEGER NOT NULL, "
                       "UNIQUE(object_id, poly_index), "
                       "FOREIGN KEY(object_id) REFERENCES objects(id));")
        db.commit()
    
    def db_get_obj_id(self, db_cur, obj_id_name):
        # BUG:: None of the following are working:
        #    SELECT last_insert_rowid(); // Always returns 0
        #    db_cur.lastrowid # Always None
        
        # Although this is not efficient at all, it does work:
        db_cur.execute(
            "SELECT id FROM objects WHERE id_name=\"{}\";" \
            .format(obj_id_name))
        return db_cur.fetchone()[0]

    def db_get_mat_id(self, db_cur, mat_name):
        db_cur.execute(
            "SELECT id FROM materials WHERE name=\"{}\";" \
            .format(mat_name))
        return db_cur.fetchone()[0]

    def db_get_modifier_id(self, db_cur, modif_name, obj_id):
        db_cur.execute(
            "SELECT id FROM modifiers WHERE name=\"{}\" AND object_id={};" \
            .format(modif_name, obj_id))
        return db_cur.fetchone()[0]
    
    def db_get_poly_id(self, db_cur, poly, obj_id):
        db_cur.execute(
            "SELECT id FROM polys WHERE poly_index={} AND object_id={};" \
            .format(poly.index, obj_id))
        return db_cur.fetchone()[0]
    
    def db_insert_poly(self, db_cur, poly, obj_id):
        """Insert object polygon parameters into the database."""
        try:
            use_smooth = 0
            if poly.use_smooth:
                use_smooth = 1
            
            cmd = "INSERT INTO polys" \
                  "(object_id, poly_index, mat_index, use_smooth) " \
                  "VALUES({}, {}, {}, {});" \
                  .format(
                    obj_id, poly.index, 
                    poly.material_index, use_smooth
                  )
            db_cur.execute(cmd)
        except sqlite3.IntegrityError as e:
            pass
    
    def db_insert_object(self, db_cur, obj):
        """Insert an object into the database."""
        try:
            cmd = "INSERT INTO objects(id_name, name) " \
                  "VALUES(\"{}\", \"{}\");".format(obj.id, obj.name)
            db_cur.execute(cmd)
            
            obj_id = self.db_get_obj_id(db_cur, obj.id)
            
            # Only store polygon params for objects with multiple materials.
            if len(obj.material_slots) > 1:
                for p in obj.data.polygons:
                    self.db_insert_poly(db_cur, p, obj_id)
            return obj_id
        except sqlite3.IntegrityError:
            pass
        return self.db_get_obj_id(db_cur, obj.id)

    def db_insert_material(self, db_cur, mat):
        """Insert a material into the database."""
        try:
            mat_d = bl_to_dict(mat)
            cmd = "INSERT INTO materials(name, code) " \
                  "VALUES(\"{}\", \"{}\");".format(mat.name, str(mat_d))
            db_cur.execute(cmd)
        except sqlite3.IntegrityError:
            pass
        return self.db_get_mat_id(db_cur, mat.name)
    
    def db_link_material(self, db_cur, obj_id, mat_id):
        """Insert a material to object reference into the database."""
        try:
            cmd = "INSERT INTO material_map(object_id, material_id) " \
                  "VALUES({}, {});" \
                  .format(obj_id, mat_id)
            db_cur.execute(cmd)
        except sqlite3.IntegrityError:
            pass

    def db_insert_modifier(self, db_cur, modif, obj_id):
        """Insert an object modifier into the database."""
        try:
            modif_d = bl_to_dict(modif)
            cmd = "INSERT INTO modifiers(name, type, object_id, code) " \
                  "VALUES(\"{}\", \"{}\", {}, \"{}\");" \
                  .format(modif.name, modif.type, obj_id, str(modif_d))
            db_cur.execute(cmd)
        except sqlite3.IntegrityError as e:
            pass
        #return self.db_get_modifier_id(db_cur, modif, obj_id)

    def execute(self, context):
        db_cur = None
        try:
            # Store object UIDs somewhere in the Blender file.
            self.store_uids(context)
            
            # Connect to the assembly database.
            db, db_cur = self.db_connect(context)
            # Create tables in the database.
            self.db_create_tables(db, db_cur)
            
            # Loop through objects.
            for o in context.scene.objects:
                obj_id = self.db_insert_object(db_cur, o)
            
                # Store materials in the database.
                for m in o.material_slots:
                    print("> Obj {}, {}: Material {}".format(o.id, o.name, m.name))
                    mat_id = self.db_insert_material(db_cur, m.material)
                    self.db_link_material(db_cur, obj_id, mat_id)
                
                # Store all its modifiers.
                for m in o.modifiers:
                    print("> Obj {}, {}: Modifier {}".format(o.id, o.name, m.name))
                    self.db_insert_modifier(db_cur, m, obj_id)
            
            # Commit changes to the database.
            db.commit()
        except Exception as e:
            print(traceback.format_exc())
            if db_cur:
                db_cur.close()
            return {'CANCELLED'}
        if db_cur:
            db_cur.close()
        return {'FINISHED'}


class STLAssemblyLoadOperator(Operator):
    bl_idname = "import_stl_asm.load"
    bl_label = "Load materials and modifiers from the database " \
               "and apply them to the assembly."
    
    def store_uids(self, context):
        wm = context.window_manager
        wm["objects"] = 0
        rna = wm.get("_RNA_UI", {})
        rna["objects"] = {o.name: o.id for o in bpy.data.objects}
        wm["objects"] = len(rna["objects"])
        wm["RNA_UI"] = rna
    
    def db_connect(self, context):
        path = context.scene.stl_asm_import.path_db
        path = os.path.abspath(bpy.path.abspath(path))
        print(path)
        db = sqlite3.connect(path)
        db_cur = db.cursor()
        return db, db_cur
    
    def db_get_obj_name(self, db_cur, id_name):
        """Find object name, based on its ID (original name)."""
        db_cur.execute(
            "SELECT name FROM objects WHERE id_name=\"{}\";" \
            .format(id_name))
        result = db_cur.fetchone()
        if result:
            return result[0]
        return None
    
    def db_get_materials(self, db_cur, obj_id_name):
        db_cur.execute(
            "SELECT m.name, m.code FROM objects o, materials m, material_map mm " \
              "WHERE o.id_name=\"{}\" AND mm.object_id=o.id AND m.id=mm.material_id;" \
              .format(obj_id_name))
        return db_cur.fetchall()
        
    def db_get_modifiers(self, db_cur, obj_id_name):
        db_cur.execute(
            "SELECT m.name, m.type, m.code FROM objects o, modifiers m " \
            "WHERE o.id_name=\"{}\" AND m.object_id=o.id;" \
            .format(obj_id_name))
        return db_cur.fetchall()
        
    def db_get_polys(self, db_cur, obj_id_name):
        db_cur.execute(
            "SELECT p.poly_index, p.mat_index, p.use_smooth " \
            "FROM objects o, polys p " \
            "WHERE o.id_name=\"{}\" AND p.object_id=o.id;" \
              .format(obj_id_name))
        return db_cur.fetchall()
    
    def make_materials(self, object, materials):
        """Create Blender materials, based on 
           the material info from the database."""
        retval = []
        if materials:
            for mat in materials:
                mat_name, mat_code, = mat
                if mat_name in bpy.data.materials.keys():
                    print("Updating material {}".format(mat_name))
                    m = bpy.data.materials.get(mat_name)
                else:
                    print("Creating material {}".format(mat_name))
                    m = bpy.data.materials.new(mat_name)
                
                m = dict_to_bl(m, eval(mat_code))
                
                if mat_name not in object.data.materials:
                    print("Linking material {} to object {}" \
                          .format(mat_name, object.name))
                    object.data.materials.append(m)
                
                retval.append(m)
        return retval
    
    def make_modifiers(self, object, modifiers):
        """Create Blender modifiers, based on
           the info from the database."""
        retval = []
        if modifiers:
            for modif in modifiers:
                m_name, m_type, m_code, = modif
                if m_name in object.modifiers:
                    retval.append(object.modifiers.get(m_name))
                    print("Modifier {} already exists".format(m_name))
                else:
                    print("Assigning modifier {} to {}".format(m_name, object.name))
                    
                    m = object.modifiers.new(m_name, m_type)
                    m = dict_to_bl(m, eval(m_code))
                    retval.append(m)
        return retval
    
    def update_polys(self, object, polys):
        """Update Blender object polygons, based on 
           the polygon parameters from the database."""
        failed = []
        retval = []
        if polys:
            for p in polys:
                p_index, p_mat_index, p_use_smooth, = p
                try:
                    object.data.polygons[p_index].material_index = p_mat_index
                    object.data.polygons[p_index].use_smooth = p_use_smooth
                except Exception as e:
                    failed.append(p_index)
        if failed:
            print("Failed to update polys {}".format(failed))
        return retval
    
    def execute(self, context):
        db_cur = None
        try:
            # Store object UIDs somewhere in the Blender file.
            self.store_uids(context)
            
            # Connect to the assembly database.
            db, db_cur = self.db_connect(context)
            
            # Loop through objects.
            for o in context.scene.objects:
                # Update object names.
                new_name = self.db_get_obj_name(db_cur, o.id)
                # An object 
                if not new_name:
                    print("Object \"{}\" not in the database.".format(o.id))
                    continue
                if o.id != new_name:
                    print("Renaming {} -> {}".format(o.id, new_name))
                    o.name = new_name
                # Update polygon params.
                polys = self.db_get_polys(db_cur, o.id)
                self.update_polys(o, polys)
                
                # Make materials and link them to the object.
                materials = self.db_get_materials(db_cur, o.id)
                self.make_materials(o, materials)
                # Apply modifiers to objects.
                modifiers = self.db_get_modifiers(db_cur, o.id)
                self.make_modifiers(o, modifiers)
            
        except Exception as e:
            print(traceback.format_exc())
            if db_cur:
                db_cur.close()
            return {'CANCELLED'}
        if db_cur:
            db_cur.close()
        return {'FINISHED'}


class STLAssemblyApplyOperator(Operator):
    bl_idname = "import_stl_asm.apply"
    bl_label = "Apply all modifiers."
    
    def execute(self, context):
        # Loop through objects.
        for o in context.scene.objects:
            if o.type == 'MESH':
                print("Applying modifiers on \"{}\"..".format(o.name))
                
                bm = bmesh.new()
                bm.from_object(o, context.scene, apply_modifiers=True)
                bm.to_mesh(o.data)
                bm.free()
                
                o.modifiers.clear()
        print("Modifiers applied.")
        
        return {'FINISHED'}


class STLAssemblyImportSettings(PropertyGroup):
    path = StringProperty(
           name="",
           description="Path to directory with STL files",
           default="",
           maxlen=1024,
           subtype='DIR_PATH')

    path_db = StringProperty(
              name="",
              description="Path to the assembly database",
              default="",
              maxlen=1024,
              subtype='FILE_PATH')


class STLAssemblyImportPanel(Panel):
    """This appears in the tooltip of the operator and in the generated docs"""
    bl_idname = "import_stl_asm"
    bl_label = "Import STL Assembly"
    bl_space_type = "VIEW_3D"
    bl_region_type = "TOOLS"
    bl_category = "Tools"
    bl_context = "objectmode"
    
    def draw(self, context):
        """Draw the panel with all its widgets."""
        col = self.layout.column(align=True)
        
        col.prop(context.scene.stl_asm_import, "path", text="")
        col.operator(STLAssemblyImportOperator.bl_idname, text="Import")
        col.operator(STLAssemblyProcessOperator.bl_idname, text="Process")
        
        col.separator()
        col.prop(context.scene.stl_asm_import, "path_db")
        col.operator(STLAssemblySaveOperator.bl_idname, text="Save")
        col.operator(STLAssemblyLoadOperator.bl_idname, text="Load")
        col.operator(STLAssemblyApplyOperator.bl_idname, text="Apply")


def register():
    """Register this script."""
    bpy.utils.register_module(__name__)
    bpy.types.Scene.stl_asm_import = PointerProperty(
        type=STLAssemblyImportSettings)


def unregister():
    """Deregister ourselves."""
    bpy.utils.unregister_module(__name__)
    del bpy.types.Scene.stl_asm_import


# https://blenderartists.org/forum/showthread.php?328009-Unique-object-id


def make_key(obj):
    """Unique-ID, which is identical to the original name of the object."""
    return obj.name


def get_id(self):
    """Get the Unique-ID of an object."""
    if "id" not in self.keys():
        self["id"] = make_key(self)
    return self["id"]


def uid_to_obj_hack():
    """Add a Unique-ID property to all objects."""
    bpy.types.Object.id = property(get_id)


if __name__ == "__main__":
    register()
    uid_to_obj_hack()
